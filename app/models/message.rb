class Message < ApplicationRecord
  belongs_to :channel

  validates :channel, presence: true
  validates :body, presence: true
end
